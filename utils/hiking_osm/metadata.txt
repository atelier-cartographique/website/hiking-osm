# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=hiking.osm.be
qgisMinimumVersion=3.0
description=This plugin helps to prepare maps for hiking.osm.be
version=0.1
author=Champs-Libres Coopérative
email=julien.minet@champs-libres.coop

about=This plugin helps to export a layer of frame to a suitable geojson format

tracker=http://bugs
repository=http://repo
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python

homepage=http://homepage
category=Plugins
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=

# If the plugin can run on QGIS Server.
server=False

