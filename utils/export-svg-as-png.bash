#! /bin/bash
for i in svg/*MALONNE.svg
do
    filename=`basename -s .svg $i`
    inkscape -z -d=300 -e="png/$filename.png" $i
done
