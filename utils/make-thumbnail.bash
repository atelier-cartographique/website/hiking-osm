#! /bin/bash
# from https://www.preciouschicken.com/blog/posts/imagemagick-thumbnail-bash-script/
# See https://github.com/ImageMagick/ImageMagick/issues/396 if memory limit issues
# Sets the desired thumbnail width
WIDTH=1000
# Loops through all png files in current folder
for i in png/*MALONNE.png
do
    # Stores the width of the current file
    iwidth=`identify -format "%w" $i`
    iheight=`identify -format "%h" $i`
    # Checks current filename does not end with '-thumb' and
    # file is greater than desired width and is landscape
    if [[ $i != *-thumb.png ]] && [[ $iwidth -gt $WIDTH ]]   #&& [[ $iwidth -gt $iheight ]]
    then
        # Stores filename without extension
        filename=`basename -s .png $i`
        echo $filename
        if [[ $iwidth -gt $iheight ]]
        then
	    convert -limit memory 4gb -thumbnail ${WIDTH}x -border 1 -bordercolor "#444444" $i "thumbnails/$filename-thumb.png"
        else
            convert -limit memory 4gb -thumbnail x${WIDTH} -border 1 -bordercolor "#444444" $i "thumbnails/$filename-thumb.png"
        fi
    fi
done


