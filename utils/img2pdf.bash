#! /bin/bash
# Loops through all png files in current folder
for i in png/*MALONNE.png
do
    filename=`basename -s .png $i`
    convert $i -background white -alpha remove -alpha off "$filename-no-alpha.png"
    img2pdf -a -S A3 "$filename-no-alpha.png" -o "pdf/$filename.png.pdf"
    rm "$filename-no-alpha.png"
done

