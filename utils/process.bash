#! /bin/bash

WIDTH=1000

for i in svg/*.svg
do
    filenamesvg=`basename -s .svg $i`
    inkscape -z -d=300 -e="png/$filenamesvg.png" $i
done

for i in png/*.png
do
    filename=`basename -s .png $i`
    convert $i -background white -alpha remove -alpha off "$filename-no-alpha.png"
    img2pdf -a -S A3 "$filename-no-alpha.png" -o "pdf/$filename.png.pdf"
    rm "$filename-no-alpha.png"
       
    # Stores the width of the current file
    iwidth=`identify -format "%w" $i`
    iheight=`identify -format "%h" $i`
    # Checks current filename does not end with '-thumb' and
    # file is greater than desired width and is landscape
    if [[ $i != *-thumb.png ]] && [[ $iwidth -gt $WIDTH ]]   #&& [[ $iwidth -gt $iheight ]]
    then
        # Stores filename without extension
        filename=`basename -s .png $i`
        echo $filename
        if [[ $iwidth -gt $iheight ]]
        then
	    convert -limit memory 4gb -thumbnail ${WIDTH}x -border 1 -bordercolor "#444444" $i "thumbnails/$filename-thumb.png"
        else
            convert -limit memory 4gb -thumbnail x${WIDTH} -border 1 -bordercolor "#444444" $i "thumbnails/$filename-thumb.png"
        fi
    fi
done


